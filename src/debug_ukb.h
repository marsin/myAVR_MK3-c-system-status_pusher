/******************************************************************************
 Atmel - C system - status pusher
 - Pushing received status codes on LCD and UART
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Debug status codes for the USB driver development (HID keyboard).
 *
 * @file      debug_ukb.h
 * @date      2017
 * @author    Martin Singer
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */

#ifndef STATUS_PUSHER_DEBUG_UKB_H
#define STATUS_PUSHER_DEBUG_UKB_H


#define TYPE_STATUS 0x01
#define TYPE_ERROR  0x02

#define FN_ISR_GEN_VECT           0x01
#define FN_ISR_COM_VECT           0x02
#define FN_ACTIVATE_ENDPOINT_ZERO 0x11
#define FN_USB_SETUP              0x12
#define FN_SETUP_DEVICE           0x13
#define FN_SETUP_DESCRIPTOR       0x14
#define FN_USB_CONTROL_READ       0x15

#define POS_START  0x01
#define POS_MIDDLE 0x02
#define POS_END    0x03

#define POS_TOKEN_SETUP              0x11
#define POS_TOKEN_OUT                0x12
#define POS_TOKEN_IN                 0x13
#define POS_TOKEN_ELSE               0x14
#define POS_TOKEN_ERR                0x15
#define POS_ACTIVE_ENDPOINT_ZERO     0x21
#define POS_RECIPIENT0               0x32
#define POS_RECIPIENT1               0x33
#define POS_RECIPIENT2               0x34
#define POS_RECIPIENT_OTHER          0x35
#define POS_REQUEST_GET_STATUS       0x41
#define POS_REQUEST_SET_ADDRESS      0x42
#define POS_REQUEST_GET_DESCRIPTOR   0x43
#define POS_DIRECTION_HOST_TO_DEVICE 0x44
#define POS_DIRECTION_DEVICE_TO_HOST 0x45
#define POS_DIRECTION_UNKNOWN        0x46
#define POS_REQUEST_UNKNOWN          0x47
#define POS_DEVICE_DESCRIPTOR        0x51
#define POS_CONF_DESCRIPTOR          0x52
#define POS_STRING_DESCRIPTOR        0x53
#define POS_INTERFACE_DESCRIPTOR     0x54
#define POS_ENDPOINT_DESCRIPTOR      0x55
#define POS_UNKNOWN_DESCRIPTOR       0x56
#define POS_HOSTS_WANTS_TO_SEND      0x61

#define MSG_GENERAL_ERROR            -128 // If an error happened, but no specific error code is available
#define MSG_OK                          0 // If everything works fine
#define MSG_REPORT_MARK               126 // If there is no error or status code, just a report mark
#define MSG_FUNCTION_NOT_IMPLEMENTED  127 // If a program tree becomes arrived, but the function is not implemented yet


#endif // STATUS_PUSHER_DEBUG_UKB_H

