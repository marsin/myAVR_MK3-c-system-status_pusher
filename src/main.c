/******************************************************************************
 Atmel - C system - status pusher
 - Pushing received status codes on LCD and UART
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Main program.
 *
 * @file      main.c
 * @date      2017
 * @author    Martin Singer
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */

#ifndef F_CPU
#  define F_CPU 16000000ul
#endif
#ifndef B_UART
#  define B_UART 9600ul
#endif
#ifndef TWI_ADDR
#  define TWI_ADDR 0x30
#endif
#ifndef TWI_BUFFER_MAX
#  define TWI_BUFFER_MAX 0x81
#endif


#include <stdio.h>
#include <string.h>
#include <avr/interrupt.h>
#include <util/twi.h>
#include <util/delay.h>
#include <mars/mk3/leds.h>
#include <mars/mk3/lcd.h>
#include <mars/mk3/seg7.h>
#include <mars/mk3/switch.h>
#include <mars/mk3/joystick.h>
#include <mars/mk3/lcd_terminal.h>
#include <mars/twi/slave.h>
#include <mars/uart/uart3_irq.h>
#include <mars/uart/uart_utils.h>
#include "debug_ukb.h"


#define NO_NEW_DATA_AVAILABLE 0
#define NEW_DATA_AVAILABLE 1
#define STATE_MSG_START_CHAR '\a' // 0x07, BEL


// Prototypes
void init(void);
int twi_getchar(FILE*);
void twi_slave_call_me(uint8_t);
int print_debug_message(char*);


// Global Variables
FILE lcd_stream = FDEV_SETUP_STREAM(mk3_lcd_terminal_putchar, NULL, _FDEV_SETUP_WRITE);
FILE twi_stream = FDEV_SETUP_STREAM(NULL, twi_getchar, _FDEV_SETUP_READ);
FILE uart_stream = FDEV_SETUP_STREAM(uart3_irq_putchar, uart3_irq_getchar_wait, _FDEV_SETUP_RW);

volatile uint8_t flag_twi_data = NO_NEW_DATA_AVAILABLE;


/** Init the system.
 *
 * Procedure
 * ---------
 *
 * - Init all board devices
 * - Init the TWI as slave in EEPROM mode
 *   for communicating with the remote system which is to debug
 * - Init the UART *(not yet)*
 *   for communicating with the PC for instructions
 * - Connect TWI slave callback function
 *   which is called from the `ISR(TWI_vect)`
 * - Setup standard in, out and error stream
 * - Init buffer and flags
 * - Turn on LCD light and print ready message
 */
void init(void)
{
	mk3_led_init();
	mk3_seg7_init();
	mk3_switch_init();
	mk3_joystick_init();
	mk3_lcd_terminal_init();

	twi_slave_init(TWI_ADDR, TWI_BUFFER_MAX, TWI_SLAVE_MODE_ADDRESS);
	uart3_irq_init(uart_calc_ubrr(F_CPU, B_UART));

	twi_slave_connect_callback(&twi_slave_call_me);

	stdin = &twi_stream;
//	stdout = &lcd_stream;
	stdout = &uart_stream;
	stderr = &lcd_stream;

	// init twi buffer
	for (uint8_t i = TWI_BUFFER_MAX; i > 0; --i) {
		twi_slave_buffer.data[i] = '\0';
	}
	twi_slave_buffer.data[0] = '\0';

	mk3_lcd_light(1);
	fprintf(stdout, "Pusher ready!\n");
	fprintf(stderr, "Pusher ready!\n");
}


/** Main function.
 *
 * About
 * -----
 *
 *  - twi and uart require interrupt
 *
 *
 * Procedure
 * ---------
 *
 *  - Call init function
 *  - Turn on interrupts
 *  - Main loop
 */
int main(void)
{
	char string[TWI_BUFFER_MAX];

	init();
	sei();

	while (!0) {
		if (flag_twi_data == NEW_DATA_AVAILABLE) {
			flag_twi_data = NO_NEW_DATA_AVAILABLE;
//			gets(string);
//			puts(string); //TEST
//			mk3_led_set_output(0xF0);
			print_debug_message((char*) twi_slave_buffer.data);
//			_delay_ms(100);
//			mk3_led_set_output(0x00);
			/*
			if (string[0] == STATE_MSG_START_CHAR) {
				print_status_code(string);
			} else {
				puts(string);
			}
			*/
		}
	}
	return 0;
}


/** <stdio.h> conform getchar() function */
int twi_getchar(FILE *stream)
{
	static uint8_t pos = 0;
	char c = twi_slave_buffer.data[pos];

	if (c == '\0' || pos >= TWI_BUFFER_MAX) {
		pos = 0;
	} else {
		++pos;
	}

	return (int) c;
}


/** Callback function connected to TWI slave.
 *
 * This function becomes called in the end of ISR(TWI_vect).
 * This means it is part of a ISR and has to be as quick as possible.
 */
void twi_slave_call_me(uint8_t const tw_status)
{
	switch (tw_status) {
		case TW_SR_STOP: // 0xA0: TWI Slave Receiver - stop or repeated start condition received while selected
			flag_twi_data = NEW_DATA_AVAILABLE;
			break;
		default:
			// do nothing
			break;
	}
}


/** */
int print_debug_message(char *str_msg_code)
{
//	puts(str_code); //TEST

	if (strlen(str_msg_code) != 5) {
		printf("strlen: %i\n", strlen(str_msg_code));
		return -1;
	}

	// str_code[0] is STATE_MSG_START_CHAR ('\a', 0x07)
	uint8_t type = (uint8_t) str_msg_code[1];
	uint8_t fn   = (uint8_t) str_msg_code[2];
	uint8_t pos  = (uint8_t) str_msg_code[3];
	int8_t  msg  = (int8_t)  str_msg_code[4];
	// str_code[5] is EOL ('\0', 0x00)

	switch (type) {
		case TYPE_STATUS:
//			fputs("S ", stdout);
			printf("Status: ");
			break;
		case TYPE_ERROR:
//			fputs("E ", stdout);
			printf("Error:  ");
			break;
		default:
			printf("Unknown Message Type: '%x'", type);
//			fputs("unknown type", stdout);
			break;
	}

	switch (fn) {
		case FN_ISR_GEN_VECT:
			fputs("I:GEN  ", stdout);
			break;
		case FN_ISR_COM_VECT:
			fputs("I:COM  ", stdout);
			break;
		case FN_ACTIVATE_ENDPOINT_ZERO:
			fputs("F:AEPZ ", stdout);
			break;
		case FN_USB_SETUP:
			fputs("F:USBS ", stdout);
			break;
		case FN_SETUP_DEVICE:
			fputs("F:SDEV ", stdout);
			break;
		case FN_SETUP_DESCRIPTOR:
			fputs("F:SDES ", stdout);
			break;
		case FN_USB_CONTROL_READ:
			fputs("F:CONR ", stdout);
			break;
		default:
			printf("F%x ", fn);
//			fputs("unknown function", stdout);
			break;
	}

	switch (pos) {
		case POS_TOKEN_SETUP:
			fputs("P:T-SETU ", stdout);
			break;
		case POS_TOKEN_OUT:
			fputs("P:T-OUT  ", stdout);
			break;
		case POS_TOKEN_IN:
			fputs("P:T-IN   ", stdout);
			break;
		case POS_TOKEN_ELSE:
			fputs("P:T-ELSE ", stdout);
			break;
		case POS_TOKEN_ERR:
			fputs("P:T-ERR  ", stdout);
			break;
		case POS_ACTIVE_ENDPOINT_ZERO:
			fputs("P:ACTEPZ ", stdout);
			break;
		case POS_RECIPIENT0:
			fputs("P:REC-R0 ", stdout);
			break;
		case POS_RECIPIENT1:
			fputs("P:REC-R1 ", stdout);
			break;
		case POS_RECIPIENT2:
			fputs("P:REC-R2 ", stdout);
			break;
		case POS_RECIPIENT_OTHER:
			fputs("P:REC-O  ", stdout);
			break;
		case POS_REQUEST_GET_STATUS:
			fputs("P:REQ-GS ", stdout);
			break;
		case POS_REQUEST_SET_ADDRESS:
			fputs("P:REQ-SA ", stdout);
			break;
		case POS_REQUEST_GET_DESCRIPTOR:
			fputs("P:REQ-GD ", stdout);
			break;
		case POS_DIRECTION_HOST_TO_DEVICE:
			fputs("P:DIR-HD ", stdout);
			break;
		case POS_DIRECTION_DEVICE_TO_HOST:
			fputs("P:DIR-DH ", stdout);
			break;
		case POS_DIRECTION_UNKNOWN:
			fputs("P:DIR-UK ", stdout);
			break;
		case POS_REQUEST_UNKNOWN:
			fputs("P:REQ-UK ", stdout);
			break;
		case POS_DEVICE_DESCRIPTOR:
			fputs("P:DEV-DS ", stdout);
			break;
		case POS_CONF_DESCRIPTOR:
			fputs("P:CON-DS ", stdout);
			break;
		case POS_STRING_DESCRIPTOR:
			fputs("P:STR-DS ", stdout);
			break;
		case POS_INTERFACE_DESCRIPTOR:
			fputs("P:INT-DS ", stdout);
			break;
		case POS_ENDPOINT_DESCRIPTOR:
			fputs("P:EP-DS  ", stdout);
			break;
		case POS_UNKNOWN_DESCRIPTOR:
			fputs("P:UK-DS  ", stdout);
			break;
		case POS_HOSTS_WANTS_TO_SEND:
			fputs("P:HWTS   ", stdout);
			break;
	}

	printf("%i \n", msg);
	return 0;
}

